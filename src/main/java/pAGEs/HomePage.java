package pAGEs;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdmethods.ProjectMethods;

public class HomePage extends ProjectMethods {
	
	public HomePage()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.LINK_TEXT, using = "CRM/SFA")
	WebElement eleCRMsfa;
	@FindBy(how=How.CLASS_NAME, using = "decorativeSubmit")
	WebElement eleLogout;

		public MyHomePage clicCrmsfa()
		{
			
			click(eleCRMsfa);
			return new MyHomePage();
			
		}
	
}